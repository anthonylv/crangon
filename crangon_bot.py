#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Startup the bot controller.

This module controls the bot.
"""
import sys, os, time, signal
import settings
from reddit_account import RedditAccount

# Non_public global keeps the bot running until we receive kill signal
_stay_alive = True


def kill_handler(sig, frame):
    """Send the kill signal.
    """
    global _stay_alive
    print "Received the kill signal. Shutting down."
    _stay_alive = False


def main(argv):
    """Run the bot.

    Args:
        argv: The command line options. (Currently not used.)
    """
    global _stay_alive

    full_path = os.path.abspath(__file__)
    source_dir = os.path.dirname(full_path) + os.sep
    archive_dir = source_dir + "mail" + os.sep

    # Set the signal handler to handle SIGUSR1 from the kill_bot script
    signal.signal(signal.SIGUSR1, kill_handler)

    # Create the Reddit instance
    reddit_account = RedditAccount(settings.get_reddit_username(), settings.get_reddit_password())
    while(_stay_alive):
        unread_messages = reddit_account.get_messages_unread_count()
        print "{} has {} unread messages".format(
            settings.get_reddit_username(),
            str(unread_messages))
        time.sleep(settings.get_sleep_time())


# Program entry point
if __name__ == "__main__":
    main(sys.argv[1:])
