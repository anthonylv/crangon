#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Store general settings for the application.

Since we can't have constants, use getter functions to return settings.
"""

def get_sleep_time():
    # The bot sleeps for one hour
    return 3600

def get_reddit_username():
    return ""

def get_reddit_password():
    return ""
