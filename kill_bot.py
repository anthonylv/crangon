#! /usr/bin/python
# -*- coding: utf-8 -*-
"""Kill the bot.

This module sends the bot process a signal to set it's die flag.
"""

import psutil
import signal
import os

target = "crangon_bot.py"
target_found = False

print "Attempting to kill the bot "+target
# scan through processes
for proc in psutil.process_iter():
    try:
        pinfo = proc.as_dict(attrs=['pid', 'name', 'cmdline'])
        # Do we have the Python processes?
        if pinfo["name"] == "Python":
            #broken_path = proc.cmdline[-1].split(os.sep)
            #print "path "+broken_path
            # Index 0 contains the process name ('Python') while
            # index 1 contains the script path as an argument
            path_spearated = pinfo["cmdline"][1].split(os.sep)
            # The last item in the list is the script name itself
            script_name = path_spearated[-1]
            # if the source file name matches the target, kill the process
            if script_name == target:
                target_found = True
                print "Killing "+script_name
                proc.send_signal(signal.SIGUSR1)
    except psutil.NoSuchProcess:
        pass

if not target_found:
    print target+" isn't running."
