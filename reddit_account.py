#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Provide an interface to a user's Reddit account.

This module provides a wrapper to interfacing with a Reddic account using PRAW.
"""
import praw
import os
import requests

class RedditAccount:
    """Class to handle interaction with a Reddit account
    """

    _user_agent = ("Crangon 0.1")
    _username = ""
    _password = ""
    _reddit = None


    def __init__(self, username, password):
        print "Initializing reddit account for "+username
        self._username = username
        self._password = password
        self._reddit = praw.Reddit(user_agent=self._user_agent)
        self._reddit.login(self._username, self._password)


    def __del__(self):
        print "Goodbye"


    def get_messages_unread(self):
        """Check for unread messages.
        
        We check for unread messages and return a list of dictionaries
        containing the author, subject and body.
        """
        messages = []
        try:
            for mail in self._reddit.get_unread():
                print "Getting unread..."
                message = {
                    'author' :  mail.author.name,
                    'subject' : mail.subject,
                    'body' : mail.body
                }
                messages.append(message)
                mail.mark_as_read()

        except requests.exceptions.HTTPError as err:
            '''Service may come back online later with the following
            Error 502 - Bad Gateway 
            Error 503 - Service unavailable
            Error 504 - Gateway Timeout error
            '''
            if err.response.status_code in [502, 503, 504]:
                pass
            else:
                print str(err)
                print "Terminating the connection"
        return messages


    def get_messages_unread_count(self):
        """Check the unread message count.
        
        We check for the number of unread messages but
        messages are still marked as unread.
        """
        message_counter = 0
        try:
            for mail in self._reddit.get_unread():
                message_counter += 1

        except requests.exceptions.HTTPError as err:
            '''Service may come back online later with the following
            Error 502 - Bad Gateway
            Error 503 - Service unavailable
            Error 504 - Gateway Timeout error
            '''
            if err.response.status_code in [502, 503, 504]:
                pass
            else:
                print str(err)
                print "Terminating the connection"
        return message_counter
